/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bl;

/**
 *
 * @author Maria
 */
public class ClienteJuridico extends Cliente { 
    private ClienteContacto contacto = new ClienteContacto();  
    
    
    public ClienteJuridico(){
    }

    public ClienteJuridico(String cedula, String nombre, String provincia, String canton, String distrito, String direccion, ClienteContacto nuevoContacto ) {
        super(cedula,nombre,provincia,canton,distrito,direccion);
        this.contacto = nuevoContacto;
    }

    public ClienteContacto getContacto() {
        return contacto;
    }

    public void setContacto(ClienteContacto contacto) {
        this.contacto = contacto;
    }

}
