/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bl;

/**
 *
 * @author Maria
 */
public class Usuario {
    private String cedula;
    private String nombre;
    private String primerApellido;
    private String segundoApellido; 
    private String correo;
    private String genero;
    private String avatar; 
    
    public Usuario (){
    }
    
    public Usuario(String cedula, String nombre, String primaerApellido, String segundoApellido, String correo, String genero, String avatar) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.primerApellido = primerApellido;
        this.segundoApellido = segundoApellido;
        this.correo = correo;
        this.genero = genero;
        this.avatar = avatar;
    }
    
    public String getCedula() {
        return cedula;
    }
    
    public String getNombre() {
        return nombre;
    }
    
    public String getPrimerApellido() {
        return primerApellido;
    }
    
    public String getSegundoApellido() {
        return segundoApellido;
    }
    
    public String getCorreo() {
        return correo;
    }
    
    public String getGenero() {
        return genero;
    }
    
    public String getAvatar() {
        return avatar;
    }
    
    public void setCedula(String cedula) {
        this.cedula = cedula;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }
    
    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }
    
    public void setCorreo(String correo) {
        this.correo = correo;
    }
    
    public void setGenero(String genero) {
        this.genero = genero;
    }
    
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
    
    @Override
    public String toString() {
        return "cedula:" + cedula + 
                ", nombre:" + nombre + 
                ", primerApellido :" + primerApellido + 
                ", segundoApellido:" + segundoApellido + 
                ", correo:" + correo +
                ", genero:" + genero + 
                ", avatar:" + avatar;
        
    }
    
    
    
    
  
    
}
