/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bl;

/**
 *
 * @author Maria
 */
public class Actividad {
    private int codigo;
    private String duracionActividad;
    private String nombreActividad;
    private String descripcionActividad; 

    public Actividad(){
    }

    public Actividad(int codigo, String duracicionActividad, String nombreActividad, String descripcionActividad) {
        this.codigo = codigo;
        this.duracionActividad = duracicionActividad;
        this.nombreActividad = nombreActividad;
        this.descripcionActividad = descripcionActividad;
    }

    public int getCodigo() {
        return codigo;
    }
    
    public String getDuracion() {
        return duracionActividad;
    }

    public String getNombreActividad() {
        return nombreActividad;
    }

    public String getDescripcionActividad() {
        return descripcionActividad;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public void setDuracicionActividad(String duracicionActividad) {
        this.duracionActividad = duracicionActividad;
    }

    public void setNombreActividad(String nombreActividad) {
        this.nombreActividad = nombreActividad;
    }

    public void setDescripcionActividad(String descripcionActividad) {
        this.descripcionActividad = descripcionActividad;
    }

    @Override
    public String toString() {
        return "Actividad{" + 
                "codigo=" + codigo + 
                ", duracionActividad=" + duracionActividad + 
                ", nombreActividad=" + nombreActividad + 
                ", descripcionActividad=" + descripcionActividad + '}';
    }
    
    
    
    
    
    
    
    
    
    
    
    
}
