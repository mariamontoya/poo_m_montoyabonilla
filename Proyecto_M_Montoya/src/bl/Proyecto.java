/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bl;

import java.util.Date;

/**
 *
 * @author Maria
 */
public class Proyecto {
    private int codigo;
    private String nombre;
    private String descripcion;
    private Date fechaInicio; 
    private Date fechaFin;
    private Tecnologia tecPro; 
    private Cliente cliPro;
    private Actividad actPro;
    
    public Proyecto(){
    }
    
    public Proyecto(int codigo, String nombre, String descripcion, Date fechaInicio, Date fechaFin, Tecnologia tecPro, Cliente cliPro, Actividad actPro) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.tecPro = tecPro;
        this.cliPro = cliPro;
        this.actPro = actPro;
    }
    
    public int getCodigo() {
        return codigo;
    }
    
    public String getNombre() {
        return nombre;
    }
    
    public String getDescripcion(){
        return descripcion;
    }
    
    public Date getFechaInicio(){
        return fechaInicio;
    }
    
    public Date getFechaFin(){
        return fechaFin;
    }
    
    public Tecnologia getTecPro(){
        return tecPro;
    }
    
    public Cliente getCliPro(){
        return cliPro;
    }
    
    public Actividad getActPro(){
        return actPro;
    }
    
    public void setCodigo(int codigo){
        this.codigo = codigo;
    }
    
    public void setNombre(String nombre){
        this.nombre = nombre;
    }
    
    public void setDescripcion(String descripcion){
        this.descripcion = descripcion;
    }
    
    public void setFechaInicio(Date fechaInicio){
        this.fechaInicio = fechaInicio;
    }
    
    public void setFechaFin (Date fechaFin){
        this.fechaFin = fechaFin;
    }
    
    public void setTecPro (Tecnologia tecPro){
        this.tecPro = tecPro;
    }
    
    public void setCliPro (Cliente cliPro){
        this.cliPro = cliPro;
    }
    
    public void setActPro (Actividad actPro){
        this.actPro = actPro;
    }
    
    @Override
    public String toString() {
        return "codigo :" + codigo + 
                ",  nombre :" + nombre + 
                ", descripcion :" + descripcion + 
                ", fechaInicio :" + fechaInicio + 
                ", fechaFin :" + fechaFin + 
                ", tecPro :" + tecPro + 
                ", cliPro :" + cliPro+
                ", actPro :" + actPro;
        
    }
    
    
 
    
    
}
