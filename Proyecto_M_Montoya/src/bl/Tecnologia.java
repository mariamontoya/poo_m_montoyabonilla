/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bl;

/**
 *
 * @author Maria
 */
public class Tecnologia {
    private int codigo;
    private String nombre;
    
    public Tecnologia (){
    }
    
    public Tecnologia(int codigo, String nombre) {
        this.codigo = codigo;
        this.nombre = nombre;
    }
        
    public int getCodigo(){
        return codigo;
    }
     
    public String getNombre() {
        return nombre;
    }
     
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    @Override
    public String toString() {
        return "codigo:" + codigo + 
                ", nombre:" + nombre;
        
    }

    
   
    
}
