/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bl;

/**
 *
 * @author Maria
 */
public class ClienteFisico extends Cliente{
    private String primerApellido;
    private String segundoApellido; 
    private String correo;
    private String telefono;
    
    public ClienteFisico(){
    }

    public ClienteFisico(String cedula, String nombre, String primerApellido, String segundoApellido, String provincia, String canton, String distrito, String direccion, String correo, String telefono) {
        this.primerApellido = primerApellido;
        this.segundoApellido = segundoApellido;;
        this.correo = correo;
        this.telefono = telefono;
    }
    

    public String getPrimerApellido() {
        return primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }
    
    public String getCorreo() {
        return correo;
    }

    public String getTelefono() {
        return telefono;
    }
   
    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }
    
    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }
   
    
    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Override
    public String toString() {
        return "ClienteFisico{" + 
                ", primerApellido=" + primerApellido + 
                ", segundoApellido=" + segundoApellido + 
                ", correo=" + correo + 
                ", telefono=" + telefono + '}';
    }

    
}
