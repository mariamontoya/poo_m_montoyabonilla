/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bl;

/**
 *
 * @author Maria
 */
public class ClienteContacto {
    private String cedulaContacto;
    private String nombreContacto;
    private String primerApellidoContacto;
    private String segundoApellidoContacto;
    private String correoContacto;
    private String telefonoContacto;
    
    public ClienteContacto(){
    }

    public ClienteContacto(String cedulaContacto, String nombreContacto, String primerApellidoContacto, String segundoApellidoContacto, String correoContacto, String telefonoContacto) {
        this.cedulaContacto = cedulaContacto;
        this.nombreContacto = nombreContacto;
        this.primerApellidoContacto = primerApellidoContacto;
        this.segundoApellidoContacto = segundoApellidoContacto;
        this.correoContacto = correoContacto;
        this.telefonoContacto = telefonoContacto;
    }

    public String getCedulaContacto() {
        return cedulaContacto;
    }

    public String getNombreContacto() {
        return nombreContacto;
    }

    public String getPrimerApellidoContacto() {
        return primerApellidoContacto;
    }

    public String getSegundoApellidoContacto() {
        return segundoApellidoContacto;
    }

    public String getCorreoContacto() {
        return correoContacto;
    }

    public String getTelefonoContacto() {
        return telefonoContacto;
    }

    public void setCedulaContacto(String cedulaContacto) {
        this.cedulaContacto = cedulaContacto;
    }

    public void setNombreContacto(String nombreContacto) {
        this.nombreContacto = nombreContacto;
    }

    public void setPrimerApellidoContacto(String primerApellidoContacto) {
        this.primerApellidoContacto = primerApellidoContacto;
    }

    public void setSegundoApellidoContacto(String segundoApellidoContacto) {
        this.segundoApellidoContacto = segundoApellidoContacto;
    }

    public void setCorreoContacto(String correoContacto) {
        this.correoContacto = correoContacto;
    }

    public void setTelefonoContacto(String telefonoContacto) {
        this.telefonoContacto = telefonoContacto;
    }

    @Override
    public String toString() {
        return "ClienteContacto{" + 
                "cedulaContacto=" + cedulaContacto + 
                ", nombreContacto=" + nombreContacto + 
                ", primerApellidoContacto=" + primerApellidoContacto + 
                ", segundoApellidoContacto=" + segundoApellidoContacto + 
                ", correoContacto=" + correoContacto + 
                ", telefonoContacto=" + telefonoContacto + '}';
    }
    
    
    
    
    
    
    
    
    
    
    
}
