/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bl;

/**
 *
 * @author Maria
 */
public class Cliente {
    private String cedula;
    private String nombre;
    private String provincia;
    private String canton; 
    private String distrito;
    private String direccion; 

    public Cliente() {
    }

    public Cliente(String cedula, String nombre, String provincia, String canton, String distrito, String direccion) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.provincia = provincia;
        this.canton = canton;
        this.distrito = distrito;
        this.direccion = direccion;
    }
    
    public String getCedula() {
        return cedula;
    }
    
    public String getNombre() {
        return nombre;
    }
    
    public String getProvincia() {
        return provincia;
    }
    
    public String getCanton() {
        return canton;
    }
    
    public String getDistrito() {
        return distrito;
    }
    
    public String getDireccion() {
        return direccion;
    }
    
    public void setCedula(String cedula) {
        this.cedula = cedula;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }
    
    public void setCanton(String canton) {
        this.canton = canton;
    }
    
    public void setDistrito(String destrito) {
        this.distrito = destrito;
    }
    
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    
    @Override
    public String toString() {
        return "cedula :" + cedula + 
                ", nombre :" + nombre + 
                ", provincia :" + provincia + 
                ", canton :" + canton + 
                ", distrito :" + distrito + 
                ", direccion :" + direccion;
        
    }
}
